#include <Arduino.h>

void save();

void setup()
{
    pinMode(13, OUTPUT);
    pinMode(12, INPUT);
    attachInterrupt(
        digitalPinToInterrupt(10),
        save,
        RISING
    );
}

void loop()
{
}

void save()
{
    digitalWrite(13, digitalRead(12));
}