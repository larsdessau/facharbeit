#!/usr/bin/python

import socket


#TCP_IP = "192.168.178.44"
TCP_IP = "127.0.0.1"
#TCP_IP = "10.86.255.43"
TCP_PORT = 5005
BUFFER_SIZE = 2

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
while True:
 for i in range(1):
  data = s.recv(BUFFER_SIZE)
  data = [ord(x) for x in data]
  print(str(data[0])+ " "+str(data[1]))
s.close()
