#!/usr/bin/python

import socket
import math

TCP_IP = "127.0.0.1"
TCP_PORT = 5005
MESSAGE = chr(10) 

def msg(n):
  n = int(n)
  m = ""
  m += chr(abs(n))
  if n > 0:
    m += chr(0)
  else:
    m += chr(255)
  return m

x = 0.0
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((TCP_IP, TCP_PORT))
sock.listen(10)
conn, addr = sock.accept()
rising = True
while True:
  conn.send(msg(x))
  if rising:
    x += 1.0
  else:
    x -= 1.0
  if abs(x) >= 100.0:
    rising = not rising
