extern crate piston;
extern crate graphics;
extern crate glutin_window;
extern crate opengl_graphics;

use std::f64;
use std::sync::{Mutex, Arc};

//use piston::window::WindowSettings;
//use piston::event_loop::*;
use piston::input::*;
//use glutin_window::GlutinWindow as Window;
use opengl_graphics::GlGraphics;

#[derive(Copy, Clone)]
pub struct Parameter {
    pub voltage_range: f64,
    pub timebase: f64,
    pub x_divs: usize,
    pub y_divs: usize,
    pub num_points: Option<usize>, // usefull on old hardware
}

pub struct App {
    pub gl: GlGraphics, // OpenGL drawing backend.
}

const GREEN: [f32; 4] = [0.0, 1.0, 0.0, 1.0];
const BLACK: [f32; 4] = [0.0, 0.0, 0.0, 1.0];
const GREY: [f32; 4] = [0.2, 0.2, 0.2, 1.0];

impl App {
    pub fn render(&mut self, args: &RenderArgs, parameter: &Parameter, frame_buffer : Arc<Mutex<[i16; 400]>>) {
        use graphics::*;    

        const LINE_THICKNESS: f64 = 1.0;
        const DIVISIONS: f64 = 80.0;

        //predefining some shapes, that are used many times
        let square = rectangle::square(0.0, 0.0, 2.0);

        let vertical_grid_line =
            rectangle::rectangle_by_corners(0.0, 0.0, LINE_THICKNESS, args.height as f64);

        let horizontal_grid_line =
            rectangle::rectangle_by_corners(0.0, 0.0, args.width as f64, LINE_THICKNESS);

        let mut frame = [0;400];
        {
			let f = frame_buffer.lock().unwrap();
			frame = *f;
		}

        let (x, y) = ((args.width / 2) as f64, (args.height / 2) as f64);

        self.gl.draw(args.viewport(), |c, gl| {
            // Clear the screen.
            clear(BLACK, gl);

            //the spacing between the unit lines of the graph
            let x_step = 25.0;
            let y_step = 25.0;

            let mut line = 0.0;//loop counter

            while line <= DIVISIONS {
                //draws some lines for the grid
                rectangle(
                    GREY,
                    vertical_grid_line,
                    c.transform.trans(line * x_step, 0.0),
                    gl,
                );
                rectangle(
                    GREY,
                    horizontal_grid_line,
                    c.transform.trans(0.0, line * y_step),
                    gl,
                );
                line += 1.0;
            }

            let size = 2.0;//the size of a single box TODO: make const or dynamically adjustable
            let width: f64 = args.width as f64 / 2.0; //referse to the absolute of the max x motion, ie the width
            let height : f64 = args.height as f64 / 2.0;

            let mut counter = 0.0;//guess what this does?? it counts!

            while counter < 400.0
            {
                let transform = c.transform.trans(x, y)
                    .trans( counter * size - width, frame[counter as usize] as f64);
                rectangle(GREEN, square, transform, gl);
                counter += 1.0;
            }
        });
    }
}
