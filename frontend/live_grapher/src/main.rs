extern crate piston;
extern crate graphics;
extern crate glutin_window;
extern crate opengl_graphics;

use std::thread;
use std::time;
use std::sync::mpsc::*;
use std::sync::{Mutex, Arc};
use std::io::stdin;

use std::net::{TcpStream, SocketAddr, ToSocketAddrs};
use std::io::Read;

use piston::window::WindowSettings;
use piston::event_loop::*;
use piston::input::*;
use glutin_window::GlutinWindow as Window;
use opengl_graphics::{GlGraphics, OpenGL};

mod app;
use app::*;

const START_CMD : u8 = 0;
const OPENGL_CMD : u8 = 1;
const TRIGGER_LEVEL_CMD : u8 = 2;
const SET_IP_CMD : u8 = 3;

const SEG_SIZE : usize = 1000;// the standard size of a data batch for processing

fn main() 
{
    let (from_acc, too_trans) : (Sender<[i16; SEG_SIZE]>, Receiver<[i16; SEG_SIZE]>) = channel();
	let (cli_tx, cli_rx) : (Sender<Vec<u8>>, Receiver<Vec<u8>>) = channel();
	let (cli_main_tx, cli_main_rx) : (Sender<Vec<u8>>, Receiver<Vec<u8>>) = channel();
    let (trigger_tx, trigger_rx) : (Sender<[i16; 256]>, Receiver<[i16; 256]>) = channel();

	let frame_buffer : Arc<Mutex<[i16; 400]>> = Arc::new(Mutex::new([0 as i16; 400]));

    thread::spawn(|| {cli(cli_tx, cli_main_tx);});

    let mut new = true;
    let mut trigger = 0;
    let mut ip_port = SocketAddr::from(([127, 0, 0, 1], 5005));
    
    while match cli_main_rx.recv()
    {
        Ok(v) => match v[0]
        {
            START_CMD => !(v[1] == 1),
            OPENGL_CMD => match v[1]
            {
                0 => {new = true; true},
                1 => {new = false; true},
                _ => true,
            },
            TRIGGER_LEVEL_CMD => {trigger = v[1]; true},
            SET_IP_CMD => {ip_port = SocketAddr::from(([v[1], v[2], v[3], v[4]], 5005)); true},
            _ => true,
        },
        Err(e) => true,
    }{}
    println!("starting data_accumulator...");
    thread::spawn(move || { data_accumulator(from_acc, ip_port); });
    println!("done");
    println!("starting data_transformer...");
	let frame = frame_buffer.clone();
    thread::spawn(move || { data_transformer(too_trans, frame, |x : i16, y : i16| -> bool {(x < trigger as i16) & (y >= trigger as i16)}, 1 ); });
	println!("done");
    println!("starting scope...");
    scope(frame_buffer.clone(), new); // must be in main thread
}

fn scope(frame : Arc<Mutex<[i16; 400]>>, new : bool) 
{
    let opengl = if !new { OpenGL::V2_1 } else { OpenGL::V3_2 };

    // Create an Glutin window.
    let mut window: Window = WindowSettings::new("oscilloscope", [800, 500])
        .srgb(false)
        .opengl(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();

    // Create a new game and run it.
    let mut app = App 
    {
        gl: GlGraphics::new(opengl),
    };

    let parameter = Parameter 
    { //should be adjustable via the cli
        voltage_range: 1.0,
        timebase: 1000.0,
        x_divs: 10,
        y_divs: 10,
        num_points: None,
    };

    let mut events = Events::new(EventSettings::new());
    while let Some(e) = events.next(&mut window) 
    {//main graphics loop
        if let Some(r) = e.render_args() 
        {
            app.render(&r, &parameter, frame.clone());
        }
    }
}

fn cli(sender : Sender<Vec<u8>>, main_sender : Sender<Vec<u8>>) 
{
    loop 
    {
        let mut command = String::new();

        stdin().read_line(&mut command).expect("");
        let temp = command.clone();
        command = String::from(command.trim_right());

        match command.as_str() 
        {
            "test" => println!("testing cli"),
            "opengl old" => main_sender.send(vec![OPENGL_CMD, 1]).unwrap(),
            "opengl new" => main_sender.send(vec![OPENGL_CMD, 0]).unwrap(),
            "start scope" => main_sender.send(vec![START_CMD, 1]).unwrap(),
            "zero trigger" => main_sender.send(vec![TRIGGER_LEVEL_CMD, 0]).unwrap(),
            "fifty trigger" => main_sender.send(vec![TRIGGER_LEVEL_CMD, 50]).unwrap(),
            "hundret trigger" => main_sender.send(vec![TRIGGER_LEVEL_CMD, 100]).unwrap(),
            "twohundret trigger" => main_sender.send(vec![TRIGGER_LEVEL_CMD, 200]).unwrap(),
            _ => print!(""),
        }

        if command.starts_with("set ip")
        {
            let ip = to_ip(command.split_at(6).1);
            main_sender.send(vec![SET_IP_CMD, ip[0], ip[1], ip[2], ip[3]]).unwrap();
        }
		if command.starts_with("trigger ")
		{
	    	let level = u8_from( command.split_at(8).1);
			main_sender.send(vec![TRIGGER_LEVEL_CMD, level]).unwrap();
	    }
    }
}

fn to_ip(s_ip : &str) -> [u8; 4]
{
    let mut ip : [u8; 4] = [0; 4];
    let ip_string : String = s_ip.clone().to_string();
    let ip_parts : Vec<&str> = ip_string.split('.').collect();
    ip[0] = u8_from(ip_parts[0]);
    ip[1] = u8_from(ip_parts[1]);
    ip[2] = u8_from(ip_parts[2]);
    ip[3] = u8_from(ip_parts[3]);
    return ip;
}

fn u8_from(string : &str) -> u8
{
    let mut s = string.clone();
    s = s.trim();
    s.parse::<u8>().unwrap()
}

fn int_from(buff : &[u8]) -> i16
{
    return if buff[1] == 0 { buff[0] as i16 * (-1)} else { buff[0] as i16};
}

fn data_accumulator(transmitter: Sender<[i16; SEG_SIZE]>, ip_port : SocketAddr) 
{ 
	println!("hello");
    let mut buff: [u8; 2] = [0, 0];
    thread::sleep(time::Duration::from_millis(900));
    let mut stream = TcpStream::connect(ip_port).expect("unable to connect to TCP server");
    let mut data = [0; SEG_SIZE];
    let interval = 0;

	println!("starting loop");

    loop 
    {
        let mut i = 0;
        while i < SEG_SIZE
        {
            stream.read(&mut buff).unwrap();
            let value = int_from(&buff);
            data[i] = value;
			i += 1;
        }
        transmitter.send(data).unwrap();
    }

}

const DELAY : usize = 1; //tweek for performance, so that the gui thread doesnt build up the receive queue but also doesnt lag

fn data_transformer< T : Fn(i16, i16) -> bool>(receiver : Receiver<[i16; SEG_SIZE]>, frame_buffer : Arc<Mutex<[i16; 400]>>, trigger : T, time_div : usize)
{
    let mut data : [i16; SEG_SIZE];
	let mut buffer : Vec<i16> = Vec::new();
	let mut frame : [i16; 400] = [0; 400];
	let mut delay : usize = 0;

	for i in 0 .. (2 * SEG_SIZE)
	{
		buffer.push(0);
	}

    loop
    {
        data = match receiver.recv()
        {
            Ok(d) => d,
            Err(e) => continue
        };
		//so, this part of code only gets reached, if there was new data, otherwise it just skips the rest of the loop
		//this means, I can assume to have fresh data in everything that forllows
		for i in 0 .. SEG_SIZE
		{
			buffer.remove(0);
			buffer.push(data[i]);
			if trigger(buffer[SEG_SIZE], buffer[SEG_SIZE + 1])
			{
				frame = compose_frame(&buffer, time_div);
			}
		}
		if delay == 0
		{
			{
				let mut f = frame_buffer.lock().unwrap();
				*f = frame;
			}
			delay = DELAY;
		}
		delay -= 1;
	}
}

fn compose_frame(data : & Vec<i16>, inverse_density : usize) -> [i16; 400]
{
	let mut inv_d : usize = inverse_density;
	let mut frame : [i16; 400] = [0; 400];
	if data.len() <= (400 * inverse_density)
	{ //increases the frequency of sampled data points if the input would require more data than provided 
		inv_d = (data.len() as f64 / 400.0) as usize; 
	}

	for i in 0 .. 400
	{
		frame[i] = data[i * inv_d];
	}
	return frame;
}
