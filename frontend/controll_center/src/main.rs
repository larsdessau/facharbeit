use std::{
    io::{Write, stdout},
    thread,
    sync::{mpsc, Arc, Mutex},
    time::{Duration, Instant},
};

use tui::Terminal;
use tui::widgets::{Block, Borders, Chart, Axis, Dataset, GraphType};
use tui::symbols::{Marker};
use tui::style::{Color, Style};
use tui::layout::{Layout, Constraint, Direction};
use tui::backend::CrosstermBackend;

use crossterm::{
    execute,
    event::{self, Event as CEvent, KeyCode},
    terminal::{enable_raw_mode, disable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};


const DEPTH : usize = 1000;
type DataBlock = [(f64, f64); DEPTH];

enum Event<I> {
    Input(I),
    Tick,
}

fn main() -> Result<(), failure::Error> {
    //input handling
    let (tx, rx) = mpsc::channel();
    let conf = Arc::new(Mutex::new(Config::default()));
    let data = Arc::new(Mutex::new([(0.0, 0.0); DEPTH]));

    let tick_rate = Duration::from_millis(25);
    thread::spawn(move || {
        let mut last_tick = Instant::now();
        loop {
            if event::poll(tick_rate - last_tick.elapsed()).unwrap() {
                if let CEvent::Key(key) = event::read().unwrap() {
                    tx.send(Event::Input(key)).unwrap();
                }
            }
            if last_tick.elapsed() >= tick_rate {
                tx.send(Event::Tick).unwrap();
                last_tick = Instant::now();
            }
        }
    });
    //thread.spawn();
    run_ui(rx, data)?;
    Ok(())
}

const PAGE_COUNT : u8 = 2;

fn select_page(page : u8) -> u8 {
    let mut temp = page + 1;
    if temp >= PAGE_COUNT {
        temp = 0;
    }
    return temp;
}

fn test_data() -> DataBlock {
    let mut v : [(f64, f64); DEPTH] = [(0.0, 0.0); DEPTH];
    for i in 0 .. DEPTH {
        v[i] = (i as f64, i as f64);
    }
    return v;
}

fn update_data(d : DataBlock) -> DataBlock {
    let mut temp = d;
    for i in 0 .. DEPTH{
        temp[i].1 += 1.0;
        if temp[i].1 > 199.0 {
            temp[i].1 = 0.0;
        }
    }
    return temp;
}


fn run_ui(inputs : mpsc::Receiver<Event<crossterm::event::KeyEvent>>, channel : Arc<Mutex<[(f64, f64); DEPTH]>>) -> Result<(), failure::Error> {
    enable_raw_mode()?;

    let mut stdout = stdout();
    
    execute!(stdout, EnterAlternateScreen)?;

    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;


    terminal.clear()?;
    terminal.hide_cursor()?;

    let mut current_page = 0;
    let mut quit = false;

    let data = *channel.lock().unwrap();
    
    loop{
        if quit {
            break;
        }
        match inputs.recv()? {
            Event::Input(event) => match event.code {
                KeyCode::Char('q') => {
                    quit = true;
                    execute!(terminal.backend_mut(), LeaveAlternateScreen)?;
                },
                KeyCode::Tab => current_page = select_page(current_page),
                _ => {},
            },
            Event::Tick => {},
        };
        if current_page == 0 {
            terminal.draw(|mut f| {
                let chunks = Layout::default()
                    .direction(Direction::Vertical)
                    .constraints(
                        [
                            Constraint::Percentage(100)
                        ].as_ref()
                    )
                    .split(f.size());
                let block = Block::default()
                    .title("Block")
                    .borders(Borders::ALL);
                let data = [
                    Dataset::default()
                        .name("channel 1")
                        .marker(Marker::Braille)
                        .graph_type(GraphType::Line)
                        .style(Style::default())
                        .data(&data), 
                ];
                let chart = Chart::default()
                    .block(block)
                    .x_axis(
                        Axis::default()
                            .title("time")
                            .style(Style::default().fg(Color::Gray))
                            .labels_style(Style::default())
                            .bounds([0.0, 100.0])
                            .labels(&["0", "200"])
                    )
                    .y_axis(
                        Axis::default()
                            .title("voltage")
                            .style(Style::default())
                            .labels_style(Style::default())
                            .bounds([0.0, 100.0])
                            .labels(&["0", "200"])
                    )
                    .datasets(&data);
                f.render_widget(chart, chunks[0]);
            })?;
        }
        else if current_page == 1 {
            terminal.draw(|mut f| {
                let chunks = Layout::default()
                    .direction(Direction::Horizontal)
                    .constraints([Constraint::Percentage(50), Constraint::Percentage(50)].as_ref())
                    .split(f.size());
                let block = Block::default()
                    .title("first block")
                    .borders(Borders::ALL);
                let block2 = Block::default()
                    .title("second block")
                    .borders(Borders::ALL);
                f.render_widget(block, chunks[0]);
                f.render_widget(block2, chunks[1]);
            });
        }
    }
    disable_raw_mode()?;
    terminal.clear()?;
    Ok(())
}

struct Config {
    ip_addr : [u8; 4],
    time_base : u8,
}

impl Config {
    fn default() -> Config {
        Config{
            ip_addr : [0; 4],
            time_base : 1,
        }
    }
}

fn get_data(channel : DataBlock, config : Arc<Mutex<Config>>) {
    {
        *channel.get_mut().unwrap() = test_data();
    }
    loop {
        let data = *channel.lock().unwrap();
        *channel.get_mut().unwrap() = update_data(data); 
    }
}
