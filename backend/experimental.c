#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>

#include <string.h>

#define BCM2708_PERI_BASE 0x3F000000
#define GPIO_BASE (BCM2708_PERI_BASE + 0x200000) /* GPIO controller */

#define Persist

#define PAGE_SIZE (4 * 1024)
#define BLOCK_SIZE (4 * 1024)

int mem_fd;
void *gpio_map;

// I/O access
volatile unsigned *gpio;

// GPIO setup macros. Always use INP_GPIO(x) before using OUT_GPIO(x) or SET_GPIO_ALT(x,y)
#define INP_GPIO(g) *(gpio + ((g) / 10)) &= ~(7 << (((g) % 10) * 3))
#define OUT_GPIO(g) *(gpio + ((g) / 10)) |= (1 << (((g) % 10) * 3))

#define GPIO_SET *(gpio + 7)  // sets   bits which are 1 ignores bits which are 0
#define GPIO_CLR *(gpio + 10) // clears bits which are 1 ignores bits which are 0

#define GET_GPIO(g) (*(gpio + 13) & (1 << g)) // 0 if LOW, (1<<g) if HIGH

#define P0 5 //least sig bit
#define P1 6
#define P2 12
#define P3 13
#define P4 16
#define P5 19
#define P6 20
#define P7 21 //most sig bit
#define P8 26 //sign bit

#define TB 22

#define OUT 27

#define DBG 17

#define INT16 65535
#define PORT 5005

volatile unsigned char ping_buffer[512];
volatile unsigned char pong_buffer[512];
volatile unsigned char status[2];
/*
status[0] -> ping buffer
status[1] -> pong buffer

status[n] value:
    0 -> no valid data, in buffer, dont send, do write
    1 -> valid data in buffer, do send, dont write

*/

volatile unsigned int buffer[65536][9];
/*
buffer contains the current sensor data.
*/
volatile unsigned short int counter; //for the primary data acc loop
volatile int acc_trans_sync; //used to check how often counter did overflow and sync the transcoder to that

void *run_server();
void *data_server();
void setup_io();
void *data_acc();
void *data_converter();
void * data_printer();
void setup_gpio();

int main()
{
    printf("main started \n");

    counter = 0; //prevent crashing cause of uninitialized counter
    acc_trans_sync = 0;

    setup_gpio();

    status[1] = 0; //transcoder needs to know, where to begin
    status[0] = 0; //sender shall not write data in the beginning

    pthread_t data_acc_thread;
    pthread_t transcoder_thread;
    pthread_t server_thread;
    if (
        pthread_create(&data_acc_thread, NULL, data_acc, NULL) |
        pthread_create(&transcoder_thread, NULL, data_converter, NULL) |
        pthread_create(&server_thread, NULL, data_server, NULL)
    )
    {
        printf("thread building error \n");
        return -1;
    }

#ifdef Persist
    pthread_join(server_thread, NULL);
#endif

    //pthread_join(data_acc_thread, NULL);
    pthread_cancel(data_acc_thread);
    pthread_cancel(server_thread);
    pthread_cancel(transcoder_thread);
    return 0;
}

void setup_gpio()
{
    int g, rep;

    // Set up gpio pointer for direct register access
    setup_io();

    /*init all gpio pins to the needed states*/

    INP_GPIO(P0); //INP_GPIO needs to be done before OUT_GPIO can work
    INP_GPIO(P1);
    INP_GPIO(P2);
    INP_GPIO(P3);
    INP_GPIO(P4);
    INP_GPIO(P5);
    INP_GPIO(P6);
    INP_GPIO(P7);
    INP_GPIO(P8);
	INP_GPIO(TB);

    INP_GPIO(OUT);
    OUT_GPIO(OUT);

    INP_GPIO(DBG);
    OUT_GPIO(DBG);

} // main

//
// Set up a memory regions to access GPIO
//
void setup_io()
{
    /* open /dev/mem */
    if ((mem_fd = open("/dev/mem", O_RDWR | O_SYNC)) < 0)
    {
        printf("can't open /dev/mem \n");
        exit(-1);
    }

    /* mmap GPIO */
    gpio_map = mmap(
        NULL,                   //Any adddress in our space will do
        BLOCK_SIZE,             //Map length
        PROT_READ | PROT_WRITE, // Enable reading & writting to mapped memory
        MAP_SHARED,             //Shared with other processes
        mem_fd,                 //File to map
        GPIO_BASE               //Offset to GPIO peripheral
    );

    close(mem_fd); //No need to keep mem_fd open after mmap

    if (gpio_map == MAP_FAILED)
    {
        printf("mmap error %d\n", (int)gpio_map); //errno also set!
        exit(-1);
    }

    // Always use volatile pointer!
    gpio = (volatile unsigned *)gpio_map;

} // setup_io

void *data_acc()
{
    printf("started data acc\n");

    while (1)
    {
        while (!GET_GPIO(TB));
				GPIO_SET = 1 << OUT;
        buffer[counter][8] = GET_GPIO(P8);
        buffer[counter][0] = GET_GPIO(P0);
        buffer[counter][1] = GET_GPIO(P1);
        buffer[counter][2] = GET_GPIO(P2);
				GPIO_CLR = 1 << OUT;
        buffer[counter][3] = GET_GPIO(P3);
        buffer[counter][4] = GET_GPIO(P4);
        //GPIO_CLR = 1 << OUT;
        buffer[counter][5] = GET_GPIO(P5);
        buffer[counter][6] = GET_GPIO(P6);
        buffer[counter][7] = GET_GPIO(P7);
        counter++;
        acc_trans_sync++;
				while(GET_GPIO(TB));
    }
}

void *data_converter()
{
    printf("started data converter\n");
    unsigned short int internal_counter = 0;
    unsigned short int index = 0;
    status[0] = 0; //tell the server, no data available
    while (1)
    {
        while (acc_trans_sync < 256);
        if (status[0] == 0)
        {
            int number;
            for (unsigned short int i = 0; i < 256; i++)
            {
                index = i + internal_counter; //should overflow to the correct position
                number = 0; //init to zero
                number += buffer[index][0] >> (P0 - 0);
                number += buffer[index][1] >> (P1 - 1);
                number += buffer[index][2] >> (P2 - 2);
                number += buffer[index][3] >> (P3 - 3);
                number += buffer[index][4] >> (P4 - 4);
                number += buffer[index][5] >> (P5 - 5);
                number += buffer[index][6] >> (P6 - 6);
                number += buffer[index][7] >> (P7 - 7);

                if (buffer[i][8] > 0)
                    ping_buffer[(i*2)+1] = 0xff;
                else
                    ping_buffer[(i*2)+1] = 0x00;
                ping_buffer[(i*2)] = number;
            }
            status[0] = 1;
            acc_trans_sync -= 256;
            internal_counter += 256;
        }
        else if (status[1] == 0)
        {
            int number;
            for (unsigned short int i = 0; i < 256; i++)
            {
                index = i + internal_counter;
                number = 0; //init to zero
                number += buffer[index][0] >> (P0 - 0);
                number += buffer[index][1] >> (P1 - 1);
                number += buffer[index][2] >> (P2 - 2);
                number += buffer[index][3] >> (P3 - 3);
                number += buffer[index][4] >> (P4 - 4);
                number += buffer[index][5] >> (P5 - 5);
                number += buffer[index][6] >> (P6 - 6);
                number += buffer[index][7] >> (P7 - 7);

                if (buffer[i][8] > 0)
                    pong_buffer[(i*2)+1] = 0xff;
                else
                    pong_buffer[(i*2)+1] = 0x00;
                pong_buffer[(i*2)] = number;
            }
            status[1] = 1;
            acc_trans_sync -= 256;
            internal_counter += 256;
        }
    }
}

void *data_server()
{
    printf("started server\n");
    int sockfd, newsockfd, clilen;
    struct sockaddr_in serv_addr, cli_addr;
    int n;

    /* First call to socket() function */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0)
    {
        perror("ERROR opening socket");
        exit(1);
    }

    /* Initialize socket structure */
    bzero((char *)&serv_addr, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY; // just make a tcp server. usually on the address assigned to the pi
    serv_addr.sin_port = htons(PORT);

    /* Now bind the host address using bind() call.*/
    if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("ERROR on binding");
        exit(1);
    }

    /* Now start listening for the clients, here process will
      * go in sleep mode and will wait for the incoming connection
   */

    listen(sockfd, 5);
    clilen = sizeof(cli_addr);

    /* Accept actual connection from the client */
    newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);

    if (newsockfd < 0)
    {
        perror("ERROR on accept");
        exit(1);
    }

    /* If connection is established then start communicating */
    /* Write a response to the client */
    while (1)
    {
        if (status[0] == 1)
        {
            n = write(newsockfd, ping_buffer, 512);
		//for (int i = 0; i < 512; i++) ping_buffer[i] = 0;
						status[0] = 0;
        }

        if (status[1] == 1)
        {
            n = write(newsockfd, pong_buffer, 512);
		//for (int i = 0; i < 512; i++) pong_buffer[i] = 0;
						status[1] = 0;
        }
        if (n < 0)
        {
            perror("ERROR writing to socket");
            exit(1);
        }
    }
}

    /*
layout: 

main thread: 
  fill buffer with io reads

converter thread:
  convert buffer arrays to int and write into sendof queue

sender thread:
  rip throug send queue and tcp it the fuck away
*/
